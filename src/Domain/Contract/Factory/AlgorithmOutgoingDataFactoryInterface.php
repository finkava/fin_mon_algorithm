<?php

namespace Finmon\Algorithm\Domain\Contract\Factory;

use Finmon\Algorithm\Domain\Entity\DTO\Algorithm\Outgoing\CoinChangePriceData;
use Finmon\Algorithm\Domain\Entity\DTO\CoinPriceChange;

interface AlgorithmOutgoingDataFactoryInterface
{
    public function getCoinChangePriceData(
        CoinPriceChange $coinPriceChange,
        int $min,
    ): CoinChangePriceData;
}