<?php

namespace Finmon\Algorithm\Domain\Contract\Factory;

use Finmon\Algorithm\Domain\Contract\Entity\CoinPriceInterface;
use Finmon\Algorithm\Domain\Entity\DTO\CoinPriceChange;

interface CoinPriceChangeFactoryInterface
{
    public function getCoinPriceChange(
        CoinPriceInterface $startPrice,
        CoinPriceInterface $finishPrice,
        int $needPercent
    ): CoinPriceChange;
}