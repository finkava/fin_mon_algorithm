<?php

namespace Finmon\Algorithm\Domain\Contract\Algorithm;

use Finmon\Algorithm\Domain\Contract\Entity\DTO\AlgorithmIncomeDataInterface;

interface AlgorithmInterface
{
    const FUTURES_COIN_PRICE_ALGORITHM = 'coin_price';
    const ALGORITHM_LIST = [
        self::FUTURES_COIN_PRICE_ALGORITHM,
    ];
    const ALGORITHM_SETTING_NOTIFICATION = 'notification';
    const ALGORITHM_SETTING_REAL_BUY = 'real_buy';
    const ALGORITHM_SETTING_FIELDS = [
        self::ALGORITHM_SETTING_NOTIFICATION,
        self::ALGORITHM_SETTING_REAL_BUY,
    ];
    const ALGORITHM_DEFAULT_SETTING = [
        self::ALGORITHM_SETTING_NOTIFICATION => false,
        self::ALGORITHM_SETTING_REAL_BUY => false,
    ];

    public function getName(): string;
    public function handle(AlgorithmIncomeDataInterface $incomeData): void;
}
