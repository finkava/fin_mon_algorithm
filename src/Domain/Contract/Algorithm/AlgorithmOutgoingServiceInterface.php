<?php

namespace Finmon\Algorithm\Domain\Contract\Algorithm;

use Finmon\Algorithm\Domain\Contract\Entity\DTO\AlgorithmOutgoingDataInterface;

interface AlgorithmOutgoingServiceInterface
{
    public function createDemoOrder(AlgorithmOutgoingDataInterface $algorithmOutgoingData): void;
    public function createOrder(AlgorithmOutgoingDataInterface $algorithmOutgoingData): void;
    public function sendNotification(AlgorithmOutgoingDataInterface $algorithmOutgoingData): void;
}