<?php

namespace Finmon\Algorithm\Domain\Contract\Entity;

interface OrderPositionInterface
{
    public const PERCENT_TO_PROFIT = 2;
    public const TYPE_POSITION_LONG = true;
    public const TYPE_POSITION_SHORT = false;
    public const SOURCE_BINANCE_FUTURES = 'binance_futures';
}