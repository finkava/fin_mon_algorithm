<?php

namespace Finmon\Algorithm\Domain\Contract\Entity;

interface CoinPriceInterface
{
    public const RANGE_MIN = 1;
    public const RANGE_TWO_MIN = 2;
    public const RANGE_FIVE_MIN = 5;
    public const RANGE_TEN_MIN = 10;
    public const RANGE_FIFTEEN_MIN = 15;
    public const MAX_RANGE_MIN = self::RANGE_FIFTEEN_MIN;
    public const RANGE_TO_PERCENT = [
        self::RANGE_MIN => 2,
        self::RANGE_TWO_MIN => 3,
        self::RANGE_FIVE_MIN => 5,
        self::RANGE_TEN_MIN => 10,
        self::RANGE_FIFTEEN_MIN => 15,
    ];

    public function getMarketPrice(): float;
    public function getCoin(): CoinInterface;
}