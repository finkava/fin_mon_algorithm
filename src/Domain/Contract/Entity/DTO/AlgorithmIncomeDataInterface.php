<?php

namespace Finmon\Algorithm\Domain\Contract\Entity\DTO;

use Doctrine\Common\Collections\ArrayCollection;

interface AlgorithmIncomeDataInterface
{
    public function getData(): ArrayCollection;
}