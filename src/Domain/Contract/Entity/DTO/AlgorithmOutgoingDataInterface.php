<?php

namespace Finmon\Algorithm\Domain\Contract\Entity\DTO;

use Finmon\Algorithm\Domain\Entity\DTO\CoinPriceChange;

interface AlgorithmOutgoingDataInterface
{
    public function setCoinPriceChange(CoinPriceChange $coinPriceChange): self;
    public function getCoinPriceChange(): CoinPriceChange;
    public function setMin(int $min): self;
    public function getMin(): int;
}