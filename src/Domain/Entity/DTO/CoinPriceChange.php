<?php

namespace Finmon\Algorithm\Domain\Entity\DTO;

use Finmon\Algorithm\Domain\Contract\Entity\CoinInterface;
use Finmon\Algorithm\Domain\Contract\Entity\CoinPriceInterface;
use Finmon\Algorithm\Domain\Contract\Entity\OrderPositionInterface;

class CoinPriceChange
{
    private ?float $changePercent = null;
    private CoinPriceInterface $finishPrice;
    private int $needPercent;
    private CoinPriceInterface $startPrice;

    public function setStartPrice(CoinPriceInterface $coinPrice): self
    {
        $this->startPrice = $coinPrice;

        return $this;
    }

    public function setFinishPrice(CoinPriceInterface $coinPrice): self
    {
        $this->finishPrice = $coinPrice;

        return $this;
    }

    public function setNeedPercent(int $needPercent): self
    {
        $this->needPercent = $needPercent;

        return $this;
    }

    public function getChangePercent(): float
    {
        return $this->changePercent;
    }

    public function calcChangePercent(): self
    {
        $this->changePercent = $this->startPrice->getMarketPrice()
            ? (($this->startPrice->getMarketPrice() / $this->finishPrice->getMarketPrice()) * 100) - 100
            : 0;

        return $this;
    }

    public function getMarketPrice(): float
    {
        return $this->startPrice->getMarketPrice();
    }

    public function getNeedOrder(): bool
    {
        return abs($this->changePercent) >= $this->needPercent;
    }

    public function getOrderType(): bool
    {
        return $this->changePercent >= 0
            ? OrderPositionInterface::TYPE_POSITION_LONG
            : OrderPositionInterface::TYPE_POSITION_SHORT;
    }

    public function getCoin(): CoinInterface
    {
        return $this->startPrice->getCoin();
    }

    public function getPreviousMarketPrice(): float
    {
        return $this->finishPrice->getMarketPrice();
    }
}