<?php

namespace Finmon\Algorithm\Domain\Entity\DTO\Algorithm\Outgoing;

use Finmon\Algorithm\Domain\Entity\DTO\CoinPriceChange;
use Finmon\Algorithm\Domain\Contract\Entity\DTO\AlgorithmOutgoingDataInterface;

class CoinChangePriceData implements AlgorithmOutgoingDataInterface
{
    private CoinPriceChange $coinPriceChange;
    private int $min = 0;

    public function setCoinPriceChange(CoinPriceChange $coinPriceChange): self
    {
        $this->coinPriceChange = $coinPriceChange;

        return $this;
    }

    public function getCoinPriceChange(): CoinPriceChange
    {
        return $this->coinPriceChange;
    }

    public function setMin(int $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMin(): int
    {
        return $this->min;
    }
}