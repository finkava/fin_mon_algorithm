<?php

namespace Finmon\Algorithm\Application\Algorithm;

use Doctrine\Common\Collections\ArrayCollection;
use Finmon\Algorithm\Domain\Contract\Algorithm\AlgorithmInterface;
use Finmon\Algorithm\Domain\Contract\Entity\CoinPriceInterface;
use Finmon\Algorithm\Domain\Contract\Entity\DTO\AlgorithmIncomeDataInterface;
use Finmon\Algorithm\Domain\Contract\Factory\AlgorithmOutgoingDataFactoryInterface;
use Finmon\Algorithm\Domain\Contract\Factory\CoinPriceChangeFactoryInterface;
use Finmon\Algorithm\Domain\Contract\Algorithm\AlgorithmOutgoingServiceInterface;

class FuturesCoinPrice implements AlgorithmInterface
{
    public function __construct(
        private AlgorithmOutgoingServiceInterface $algorithmOutgoingService,
        private CoinPriceChangeFactoryInterface $coinPriceChangeFactory,
        private AlgorithmOutgoingDataFactoryInterface $algorithmOutgoingDataFactory,
    )
    {
    }

    public function getName(): string
    {
        return self::FUTURES_COIN_PRICE_ALGORITHM;
    }

    public function handle(AlgorithmIncomeDataInterface $incomeData): void
    {
        $this->checkOnCoinPriceChange($incomeData->getData());
    }

    private function checkOnCoinPriceChange(ArrayCollection $coinPrices): void
    {
        foreach (CoinPriceInterface::RANGE_TO_PERCENT as $min => $needPercent) {
            /* @var CoinPriceInterface $startPrice */
            $startPrice = $coinPrices->first();
            /* @var CoinPriceInterface|null $startPrice */
            $finishPrice = $coinPrices->get($min);

            $coinPriceChange = $this->coinPriceChangeFactory->getCoinPriceChange(
                $startPrice,
                $finishPrice,
                $needPercent,
            );

            if ($coinPriceChange->getNeedOrder() === false) {
                return;
            }

            $outgoingData = $this->algorithmOutgoingDataFactory->getCoinChangePriceData(
                $coinPriceChange,
                $min,
            );

            $this->algorithmOutgoingService->createDemoOrder($outgoingData);

            $this->algorithmOutgoingService->createOrder($outgoingData);

            $this->algorithmOutgoingService->sendNotification($outgoingData);
        }
    }
}