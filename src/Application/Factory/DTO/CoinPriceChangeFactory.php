<?php

namespace Finmon\Algorithm\Application\Factory\DTO;

use Finmon\Algorithm\Domain\Entity\DTO\CoinPriceChange;
use Finmon\Algorithm\Domain\Contract\Entity\CoinPriceInterface;
use Finmon\Algorithm\Domain\Contract\Factory\CoinPriceChangeFactoryInterface;

class CoinPriceChangeFactory implements CoinPriceChangeFactoryInterface
{
    public function getCoinPriceChange(
        CoinPriceInterface $startPrice,
        CoinPriceInterface $finishPrice,
        int $needPercent
    ): CoinPriceChange {
        return (new CoinPriceChange())
            ->setStartPrice($startPrice)
            ->setFinishPrice($finishPrice)
            ->setNeedPercent($needPercent)
            ->calcChangePercent();
    }
}