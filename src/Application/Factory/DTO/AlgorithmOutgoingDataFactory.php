<?php

namespace Finmon\Algorithm\Application\Factory\DTO;

use Finmon\Algorithm\Domain\Entity\DTO\Algorithm\Outgoing\CoinChangePriceData;
use Finmon\Algorithm\Domain\Entity\DTO\CoinPriceChange;
use Finmon\Algorithm\Domain\Contract\Factory\AlgorithmOutgoingDataFactoryInterface;

class AlgorithmOutgoingDataFactory implements AlgorithmOutgoingDataFactoryInterface
{
    function getCoinChangePriceData(
        CoinPriceChange $coinPriceChange,
        int $min,
    ): CoinChangePriceData
    {
        return (new CoinChangePriceData())
            ->setCoinPriceChange($coinPriceChange)
            ->setMin($min);
    }
}